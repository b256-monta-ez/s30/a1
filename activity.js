// item 2

db.fruits.aggregate([

	{$match: {onSale:true}},
	{$count: "allItemsOnSale"},
	{$out: "totalFruitsOnSale"}
]);

// item 3
db.fruits.aggregate([

	{$match: {stock: {$gte: 20}}},
	{$count: "fruitsGTE20"},
	{$out: "totalFruitsGTEto20"}
]);

// item 4
db.fruits.aggregate([

	
	{$match: {onSale:true}},
	{$group: {_id: "$supplier_id", avgPrice: {$avg: "$price"}}},
    {$out: "avgPricePerSupplier"}
])

// item 5
db.fruits.aggregate([


	{$match: {onSale:true}},
	{$group: {_id: "$supplier_id", maxPrice: {$max: "$price"}}},
	{$out: "maxPricePerSupplierOnSale"}
])

// item 6
db.fruits.aggregate([

	{$match: {onSale: true}},
	{$group: {_id: "$supplier_id", minPrice: {$min: "$price"}}},
	{$out: "minPricePerSupplierOnSale"}

])